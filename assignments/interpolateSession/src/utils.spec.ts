import type { Delimiters } from './types';
import { generateRegex } from './utils';

describe('#generateRegex', () => {
    it('should generate valid regex based on the provided delimiters', () => {
        const samples: Array<{ delimiter: Record<Delimiters, string>, regex: RegExp} > = [ 
            { delimiter: { leftDelimiter: '{', rightDelimiter: '}' }, regex: /\{(\w+)}/g },
            { delimiter: { leftDelimiter: '@@', rightDelimiter: '@@' }, regex: /\@@(\w+)@@/g },
            { delimiter: { leftDelimiter: '@', rightDelimiter: '@' }, regex: /\@(\w+)@/g },
            { delimiter: { leftDelimiter: '?', rightDelimiter: '?' }, regex: /\?(\w+)?/g },
        ];

        samples.forEach((sample) => {
            expect(generateRegex(sample.delimiter)).toEqual(sample.regex);
        })
    })
})