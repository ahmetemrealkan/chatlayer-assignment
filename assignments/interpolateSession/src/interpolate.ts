import type { Delimiters, Session } from './types'
import { generateRegex } from './utils';

const replaceAll = (value: string, regex: RegExp, data: any): string => {
    return value.replace(regex, (_, capture) => { 
        return data[capture] || '';
    });
}

export const interpolate = (value: string, session: Session = {}, options: Record<Delimiters, string>) => {
    const regex = generateRegex(options);

    return replaceAll(value, regex, session);
}