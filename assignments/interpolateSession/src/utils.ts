import type { Delimiters } from './types';

export const generateRegex = (options: Record<Delimiters, string>): RegExp => {
    const { leftDelimiter, rightDelimiter } = options;
    
    const pattern = `\\${leftDelimiter}(\\w+)${rightDelimiter}`;
    
    return RegExp(pattern, 'g');
};