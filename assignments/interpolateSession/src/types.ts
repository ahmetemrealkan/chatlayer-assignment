export type Delimiters = 'leftDelimiter' | 'rightDelimiter';
export type Session = Record<string, string>;